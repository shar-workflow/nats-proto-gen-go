module gitlab.com/shar-workflow/nats-proto-gen-go

go 1.21

require (
	github.com/nats-io/nats.go v1.30.2
	github.com/spf13/cobra v1.7.0
	github.com/yoheimuta/go-protoparser v3.4.0+incompatible
	gitlab.com/shar-workflow/shar v1.1.697
	google.golang.org/grpc v1.58.3
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/nats-io/nkeys v0.4.5 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.3.0 // indirect
)
