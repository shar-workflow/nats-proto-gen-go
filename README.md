# nats-proto-gen-go

A tool to create go wrappers for performing NATS Request/Reply from protobuf services.

### Getting Started
This assumes you have installed `go 1.21` or above, `gnumake` and `protoc` for your distribution and they are in your path.

#### Building
1. Use `git clone gitlab.com/shar-workflow/nats-proto-gen-go` to get the code.
1. Enter the directory using `cd gitlab.com/shar-workflow/nats-proto-gen-go`
1. Run `make configure` to get the go proto packages.
2. Run `make proto-gen` to build the generator.

#### Example Code
You can now optionaly build the examples from the `example/test.proto`
1. Run `make example-proto` to build the example protobuf types.
2. Finally run `make example` to build the example service and usage examples.

This creates new directories `example_output` which contains the service definition, and `nats-proto-examples`
which contain the client and server demonstration.

### Limitations
* proto-gen cannot traverse proto files, so all definitions and services must be in the same file.
* It can't generate for the built in Google types eg. empty. The expectation is that a protobuf type is used for both argument and return value.

### Usage
```
Usage:
nats-proto-gen-go [flags]

Flags:
--generate-examples         enables/disables generating example code (default true)
-h, --help                  help for nats-proto-gen-go
--message-prefix string     sets the prefix for NATS messages (eg. "MYSTREAM.Api.")
--module-namespace string   sets the local module namespace for imports (default "protogen")
--output-package string     sets the output package name (default "protogen")

```