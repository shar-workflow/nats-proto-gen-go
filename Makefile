configure: .FORCE
	go version
	go get -d google.golang.org/protobuf/cmd/protoc-gen-go@v1.31.0
	go get -d google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.3.0
	go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.31.0
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.3.0
proto-gen: .FORCE
	cd cmd/nats-proto-gen-go && go build
example-proto: .FORCE
	@echo "\033[92mBuild proto\033[0m"
	mkdir -p model
	cd example; protoc --experimental_allow_proto3_optional --go_opt=M=gitlab.com --go_out=../model test.proto;
	@echo "\033[92mCopy model\033[0m"
	mv model/gitlab.com/shar-workflow/nats-proto-gen-go/model/test.pb.go model/ && rm -rf model/gitlab.com
example: proto-gen .FORCE
	cmd/nats-proto-gen-go/nats-proto-gen-go example/test.proto --module-namespace="gitlab.com/shar-workflow/nats-proto-gen-go" --output-package="example_output"
.FORCE:
